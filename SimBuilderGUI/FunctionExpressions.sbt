<?xml version="1.0"?>
<!-- a simple template file to test expressions -->
<SMTK_AttributeManager Version="1">
  <!--**********  Category and Analysis Infomation ***********-->
  <Categories Default="General">
    <Cat>General</Cat>
  </Categories>
  <Analyses>
    <Analysis Type="Expressions">
      <Cat>General</Cat>
    </Analysis>
  </Analyses>
  <!--**********  Attribute Definitions ***********-->
  <Definitions>
    <AttDef Type="ExpressionTest" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Double Name="Value" Label="Load Curve" Version="0" AdvanceLevel="0" NumberOfRequiredValues="1">
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>

    <!--***  Expression Definitions ***-->
    <AttDef Type="PolyLinearFunction" Label="Expression" BaseType="" Version="0" Unique="true" Associations="">
      <ItemDefinitions>
        <Group Name="ValuePairs" Label="Value Pairs" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="X" Version="0" AdvanceLevel="0" NumberOfRequiredValues="0" Extensible="true"/>
            <Double Name="Value" Version="0" AdvanceLevel="0" NumberOfRequiredValues="0" Extensible="true"/>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>


  </Definitions>



  <!--**********  Attribute Instances ***********-->
  <Attributes>
  </Attributes>

  <!--********** Workflow Views ***********-->
  <RootView Title="SimBuilder">
    <DefaultColor>1., 1., 0.5, 1.</DefaultColor>
    <InvalidColor>1, 0.5, 0.5, 1</InvalidColor>
    <AdvancedFontEffects Bold="0" Italic="1" />

    <InstancedView Title="Expression Test">
      <InstancedAttributes>
        <Att Type="ExpressionTest">Expression test</Att>
      </InstancedAttributes>
    </InstancedView>

    <SimpleExpressionView Title="Functions">
      <Definition>PolyLinearFunction</Definition>
    </SimpleExpressionView>

  </RootView>
</SMTK_AttributeManager>
